export default [
  {
    id: 1,
    title: 'Como un hobby se transforma en la forma en que vives',
    slug: 'como-un-hobby-se-transforma-en-la-forma-en-que-vives',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel varius arcu. Aenean eu facilisis lectus. Aenean varius tincidunt bibendum. In hac habitasse platea dictumst. Nullam sit amet …',
    publish: '2020-05-01',
    image: {
      normal: 'https://images.unsplash.com/photo-1511027748565-a0e907fcece4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80',
      thumbnail: 'https://images.unsplash.com/photo-1511027748565-a0e907fcece4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80'
    }
  },
  {
    id: 2,
    title: 'Como un hobby se transforma en la forma en que vives',
    slug: 'como-un-hobby-se-transforma-en-la-forma-en-que-vives',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel varius arcu. Aenean eu facilisis lectus. Aenean varius tincidunt bibendum. In hac habitasse platea dictumst. Nullam sit amet …',
    publish: '2020-05-01',
    image: {
      normal: 'https://images.unsplash.com/photo-1511027748565-a0e907fcece4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80',
      thumbnail: 'https://images.unsplash.com/photo-1511027748565-a0e907fcece4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80'
    }
  }
]
