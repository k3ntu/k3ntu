import { NuxtConfig } from '@nuxt/types'
import './lib/env'

const config: NuxtConfig = {
  /*
  ** Headers of the page
  */
  head: {
    title: process.env.APP_NAME || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'k3ntu - Desarrollador de software 360º' }
    ],
    link: [
      { rel: 'icon', type: 'images/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,100;0,300;0,400;0,700;1,300;1,400;1,700&family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700&display=swap' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: {
    color: '#828285',
    height: '2px'
  },
  /*
  ** Global CSS
  */
  css: [
    '~/assets/css/main.scss',
    '~/assets/css/animations.css'
  ],
  purgeCSS: {
    whitelist: ['nuxt-progress', 'page-enter-active', 'page-leave-active', 'page-enter', 'page-leave-active']
  },
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    { src: '~/plugins/vue-html2pdf.js', mode: 'client' },
    { src: '~/plugins/qrcode-vue.js' }
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    '@nuxt/typescript-build',
    '@nuxtjs/eslint-module',
    // Doc: https://github.com/nuxt-community/nuxt-tailwindcss
    '@nuxtjs/tailwindcss',
    '@nuxtjs/composition-api/module'
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/proxy',
    'nuxt-i18n',
    '@nuxtjs/pwa'
  ],
  i18n: {
    locales: ['en', 'es'],
    defaultLocale: 'en',
    vueI18n: {
      fallbackLocale: 'en',
      messages: {
        en: {
          home: 'Home',
          podcast: 'Podcast',
          blog: 'Blog',
          portfolio: 'Portafolio'
        },
        es: {
          home: 'Inicio',
          podcast: 'Podcast',
          blog: 'Blog',
          portfolio: 'Portfolio'
        }
      }
    }
  },
  generate: {
    // choose to suit your project
    interval: 2000
  },
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
    proxy: true
  },
  proxy: {
    '/api': { target: process.env.API_URL }
  },
  /*
  ** Build configuration
  */
  build: {
    babel: {
      presets ({ isServer }) {
        return [
          [
            require.resolve('@nuxt/babel-preset-app'),
            // require.resolve('@nuxt/babel-preset-app-edge'), // For nuxt-edge users
            {
              buildTarget: isServer ? 'server' : 'client',
              corejs: { version: 3 }
            }
          ]
        ]
      }
    },
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    extend (config, ctx) {
    }
  }
}

export default config
