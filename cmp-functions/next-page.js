export const usePage = () => {
  const goBlog = (postSlug, context) => {
    context.root.$router.push({
      path: `blog/${postSlug}`
    })
  }

  return {
    goBlog
  }
}
